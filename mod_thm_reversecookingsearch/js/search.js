/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

var suggestions = new Array;
var outp;
var oldins;
var posi = -1;
var words = new Array();
var input;
var key;
var textareaArray = new Array();
//hier stehen die suchergebnisse drin:
var names = new Array();
var recipes = new Array();

function listRecipe() {
	if(textareaArray.length < 1) {
		alert("Sie haben keine Zutaten angegeben.");
	} else {
		var url = 'index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezepts.listrezepts';
		var data = 'ingredients='+JSON.stringify(textareaArray);
		
		var jsonRequest = new Request({
			url:url,
			data:data,
			method:'post',
			onSuccess:function(responseText){
			
				document.getElementById('recipes').innerHTML = responseText;
			}
		}).send();
	}
}

function searchFor(search) {
	if(search =="" ) {
		clearOutput();
		return;
	}
	
	var url = 'index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezepts.ingredientssuche';
	
	var data = 'search='+search;
	
	var request = new Request({
		url:url,
		method:'get',
		data:data,
		onSuccess:function(responseText){
			//document.getElementById('search').value = responseText;
			// Der text in "responseText" wird in ein Array mit Objekten umgewandelt.
			var erg = JSON.decode(responseText);
          // alert(responseText);
			//Läuft alle Elemente des Arrays durch 
			//und speichert die "ingname" der Objekte in das array "names"
		var str="";
			for(var i = 0; i < erg.length; i++) {
				var currentObject = erg[i];
				names[i] = currentObject.ingname;
				str = currentObject.ingname+" "+str;
			}
			suggestions = names;
			init();
		}
	}).send();
}

function init() {
	//document.getElementById('ergebnis5').innerHTML = "bla";
	outp = document.getElementById("output");
	lookAt();
}

function lookAt() {
	var ins = document.getElementsByName("search")[0].value;
	if (oldins == ins)
		return;
	else if (posi > -1)
		;
	else if (ins.length > 0) {
		words = getWord(ins);
		if (words.length > 0) {
			clearOutput();
			for ( var i = 0; i < words.length; ++i)
				addWord(words[i]);
			input = document.getElementsByName("search")[0].value;
		} else {
			posi = -1;
		}
	} else {
		posi = -1;
	}
	oldins = ins;
}

function getWord(beginning) {
	var words = new Array();
	for ( var i = 0; i < suggestions.length; ++i) {
		var j = -1;
		var correct = 1;
		while (correct == 1 && ++j < beginning.length) {
			if (suggestions[i].charAt(j) != beginning.charAt(j).toLowerCase())
				if (suggestions[i].charAt(j) != beginning.charAt(j).toUpperCase())
					correct = 0;
		}
		if (correct == 1)
			words[words.length] = suggestions[i];
	}
	return words;
}

function clearOutput() {
	while (outp.hasChildNodes()) {
		noten = outp.firstChild;
		outp.removeChild(noten);
	}
	posi = -1;
}

function addWord(word) {
	var sp = document.createElement("div");
	sp.appendChild(document.createTextNode(word));
	sp.onclick = mouseClick;
	outp.appendChild(sp);
}

var mouseClick = function() {
	document.getElementsByName("search")[0].value = this.firstChild.nodeValue;
	posi = -1;
	oldins = this.firstChild.nodeValue;
}

function addIng(){
	var search = document.getElementById('search').value;
	
	if(include(names, search)) {
		textareaArray.push(search);
		document.getElementById('textfeld').innerHTML = textareaArray;
		clearOutput();
		document.getElementById('search').value = "";
	} else {
		alert("Diese Zutat steht nicht in der Datenbank!");
	}
	
}

function include(arr, obj) {
    for(var i=0; i<arr.length; i++) {
        if (arr[i] == obj) return true;
    }
}




