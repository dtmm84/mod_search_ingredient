<?php 
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHTML::_('behavior.tooltip');

$base = JURI::base(true) . '/modules/mod_thm_reversecookingsearch/js/';
$document = JFactory::getDocument();
$document->addScript($base.'search.js');
?>

<table>
	<tr>
		<td><input type="text" name="search" id="search" onkeyup="searchFor(this.value);"/></td>
		<td>
			<input type="button" name="add" value="add" onclick="addIng();">
      	</td>
	</tr>
	<tr>
		<td>
			<div class="shadow" id="shadow">
				<div class="output" id="output"></div>
			</div>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><textarea cols="30" rows="8" name="textfeld" id="textfeld" readonly></textarea></td>
		<td></td>
	</tr>
	<tr>
		<td>
			<input type="button" id="listRecipe" name="listRecipe" value="Rezepte Suchen" onclick="listRecipe();">
		</td>
		<td></td>
	</tr>
	
</table>
<br></br>


<div id="recipes"></div>






